<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
  <title>Velocity Template Merge Tester</title>
  <style>
    div.warning-container { background-color: #ffffa0; border: 1px solid black; padding: 1em; }
    span.warning-title { font-weight: bold; font-style: italic; }
    span.warning-description { font-style: italic; }
  </style>
</head>
<body>

<h1>Velocity Template Merge Tester</h1>

<c:choose>
  <c:when test="${not empty warnings}">
    <div class="warning-container">
      <c:forEach var="warning" items="${warnings}">
        <span class="warning-title">Warning: </span><span class="warning-description">${warning}</span>
      </c:forEach>
    </div>
  </c:when>
</c:choose>

<form action="/index.html">

  <h2><label for="variables">Velocity Variables (format as Java properties file)</label></h2>
  <textarea id="variables" name="variables" rows="10" cols="100">${variablesText}</textarea>

  <h2><label for="template">Velocity Template</label></h2>
  <textarea id="template" name="template" rows="10" cols="100">${templateText}</textarea>

  <div>
    <button type="submit">Submit</button>
  </div>

</form>

<c:choose>
  <c:when test="${mergedTemplate != ''}">
    <h2>Velocity Output</h2>
    <pre>${mergedTemplate}</pre>
  </c:when>
</c:choose>

</body>
</html>
