package org.stitchy.spring;

import org.springframework.context.annotation.Configuration;

/**
 * The application level Spring configuration.
 */
@Configuration
public class ApplicationConfig
{
}
