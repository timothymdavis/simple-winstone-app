package org.stitchy.landing;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LandingPageController
{
    private static Logger LOGGER = LoggerFactory.getLogger(LandingPageController.class);

    @RequestMapping("/index.html")
    public String handleTemplateMergeRequest(
            @RequestParam(value = "variables", defaultValue = "") String variablesText,
            @RequestParam(value = "template", defaultValue = "") String templateText,
            Model model) throws ParseException, IOException
    {
        LOGGER.debug("Handling request for index.html\nvariables={}\ntemplate={}", variablesText, templateText);

        List<String> warnings = new ArrayList<String>();

        model.addAttribute("variablesText", variablesText);
        model.addAttribute("templateText", templateText);

        if (variablesText != null && templateText != null)
        {
            boolean oneOfTheKeysHasAPeriod = false;

            // Load the variables into the Velocity context.
            Properties variables = new Properties();
            StringReader variablesReader = new StringReader(variablesText);
            variables.load(variablesReader);
            VelocityContext context = new VelocityContext();
            for (Object key : variables.keySet())
            {
                key = key == null ? "" : key;
                oneOfTheKeysHasAPeriod = key.toString().contains(".");
                Object value = variables.get(key);
                LOGGER.debug("Adding key '{}' with value '{}' to context", key.toString(), value);
                context.put(key.toString(), value);
            }

            if (oneOfTheKeysHasAPeriod)
            {
                warnings.add("The '.' character has special meaning in Velocity variables.");
            }

            // Create the template.
            RuntimeServices runtimeServices = RuntimeSingleton.getRuntimeServices();
            StringReader templateReader = new StringReader(templateText);
            SimpleNode templateData = runtimeServices.parse(templateReader, "Template");
            Template template = new Template();
            template.setRuntimeServices(runtimeServices);
            template.setData(templateData);
            template.initDocument();

            // Merge the template.
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            String mergedTemplate = writer.toString();

            LOGGER.debug("Merged Template: {}", mergedTemplate);

            model.addAttribute("mergedTemplate", mergedTemplate);
        }

        model.addAttribute("warnings", warnings);

        return "index";
    }
}
